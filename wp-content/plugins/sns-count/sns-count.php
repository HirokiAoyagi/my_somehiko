<?php
/**
 * @package SNS COUNT
 * @version 1.0
 */
/*
Plugin Name: SNS Count
Description: 各記事のSNSのカウントを取得して、post_metaに保存します
Author: zuya@liginc
Version: 1.0
Author URI: http://liginc.co.jp
*/

ini_set("memory_limit", '256M');
set_time_limit(0);
define('ERR_CODE_PFILE', 100);//既に処理中
define('ERR_CODE_OWATA', 200);//通常のエラー
define('SNS_CRON_TIME_INTERVAL' , 60 * 5); // 起動の間隔
define('CRON_SCHEDULE_HANDLER', 'sns_count');
//define('ERR_CODE_PFILE', 300);


add_action("init", "set_cron_event");

// wp-cron で実行させたい処理
function cron_schedule_handler() {
	// ここに実際の処理を記述
	$obj = new SNS_Count_Execute();
	$obj->execute();

	// 指定時間後に再度起動
	$time_interval = SNS_CRON_TIME_INTERVAL;
	wp_schedule_single_event(time() + $time_interval, CRON_SCHEDULE_HANDLER);
}


function set_cron_event(){

	// wp-cron でスケジュールされていなければ、スケジュール登録
	$crons = _get_cron_array();
	$enabled = false;
	foreach ( $crons as $time => $tasks ) {
		foreach ( $tasks as $procname => $task ) {
			if ($procname === CRON_SCHEDULE_HANDLER) {
				$enabled = true; break;
			}
		}
		if ($enabled) break;
	}
	if (!$enabled)
		wp_schedule_single_event(time(), CRON_SCHEDULE_HANDLER);
	unset($tasks); unset($crons);

	// スケジュール実行
	add_action(CRON_SCHEDULE_HANDLER, 'cron_schedule_handler');

}


class SNS_Count_Execute{
	//バッチ実行フラグ
	private $exeFlg;
	private $pfile_path;
	/**
	 * 前処理
	 * 1.開始ログ
	 * 2.フラグチェック
	 */
	function __construct() {

		try{
			// 例外ハンドラを設定
			set_exception_handler(array($this, "my_exception_handler"));
			register_shutdown_function(array($this, 'shutdown'));
			$this->_writeLog('SNS更新開始');
			$this->pfile_path = dirname(__FILE__).'/pfile.txt';
			$this->check_batch_flg();
		} catch (Exception $exc) {
			if($exc->getCode() == ERR_CODE_PFILE){
				//既に処理実行中
				$this->_writeLog('例外発生：'.$exc->getMessage());
				exit;
			}else{
				//処理中の例外
				$this->end_batch_flg();
				$this->_writeLog('例外発生：'.print_r($exc,true));
				exit;
			}
		}
	}
	//終わり処理
	function __destruct() {
		$this->_writeLog('SNS更新終了');
	}
	/**
	 * 例外漏れ対応
	 * @param type $e
	 */
	function my_exception_handler($e){
		$this->end_batch_flg();
		$this->_writeLog('my_exception_handler 例外発生：'.print_r($e,true));
		$this->_writeLog('SNS更新終了');
	}
	/**
	 * シャットダウン時の処理
	 * fatalエラーを拾う
	 */
	function shutdown() {
		if (!is_null($e = error_get_last())) {
			if($e['type'] === E_ERROR || $e['type'] === E_USER_ERROR) {
				// エラー処理
				$this->end_batch_flg();
				$this->_writeLog('shutdown 例外発生：'.print_r($e,true));
				$this->_writeLog('SNS更新終了');
			}
		}
	}
	/**
	 * メイン処理
	 */
	function execute(){
		try {
			$posts = $this->getAllPost();
			$this->_writeLog('対象件数'.count($posts).'件');
			$i=1;
			foreach ($posts as $post) {
				$this->_writeLog('何'.$i.'件目の処理開始');
				$post_id = $post['ID'];
				$tar_url = get_permalink($post_id);
				//debug
				$this->debugLog('URL:'.$tar_url);
				// facebookのカウントを更新
				$this->updateFacebook($post_id, $tar_url);
				// twitterのカウントを更新
				$this->updateTweet($post_id, $tar_url);
				$this->updateHatebu($post_id, $tar_url);
				// 100件単位でリクエスト制限対策に一時停止
				if($i%100 == 0)
					usleep(500000);
				$i++;
			}
			$this->end_batch_flg();
		} catch (Exception $exc) {
			$this->end_batch_flg();
			$this->_writeLog('例外発生：'.print_r($exc,true));
			exit;
		}
	}

	/**
	 * ログ処理
	 * @param type $output
	 * @return type
	 */
	function _writeLog($output){
		$str = var_export($output, true);
		$path = dirname(__FILE__). '/log/'.date('Ym').'_log.txt';
		$fp = fopen($path, 'a');
		if($fp === FALSE) {
			return;
		}
		fwrite($fp, date('Y/m/d H:i:s') . ":");
		fwrite($fp, "{$str}\n");
		fclose($fp);
	}
	function debugLog($str){
		if(WP_DEBUG)
			$this->_writeLog($str);
	}
	/**
	 * Pファイルのチェック
	 * @throws Exception
	 */
	function check_batch_flg(){
		// すでに動いてる場合は動作させない
		$this->exeFlg = file_exists($this->pfile_path);
		if($this->exeFlg) {
			//$this->_writeLog('既に処理実行中のため終了');
			throw new Exception("既に処理実行中のため終了",ERR_CODE_PFILE);
			exit;
		}
		$this->makePFile();
	}
	/**
	 * バッチのフラグを実行前にする。
	 */
	function end_batch_flg(){
		if(file_exists($this->pfile_path)) {
			unlink($this->pfile_path);
		}
	}
	/**
	 * 記事取得
	 * @global type $wpdb
	 * @return type
	 * @throws Exception
	 */
	function getAllPost(){
		// 記事を取得して更新していく
		try{
			global $wpdb;
			return $wpdb->get_results("SELECT ID FROM ".$wpdb->posts ." WHERE post_type = 'blog' AND post_status = 'publish' " , ARRAY_A);
		} catch (Exception $e) {
			throw $e;
		}
	}
	/**
	 * URLからTwitter数取得
	 * @param type $url
	 * @return int
	 */
	function __getTweetCount($url){
		try{
			$get_twitter = 'http://urls.api.twitter.com/1/urls/count.json?url=' . urlencode($url);
			$json = $this->__getHttpData($get_twitter);
			if(empty($json)){
				return 0;
			}
			// 	_writeLog('--- twitter count ---');
			// 	_writeLog(print_r($json , true));
			$json = json_decode($json);
			$tweets = (String)$json->count;
			if(empty($tweets)) {
				$tweets = 0;
			}
			return $tweets;
		} catch (Exception $e) {
			throw $e;
		}
	}
	/**
	 * URLからいいね数取得
	 * @param type $url
	 * @return int
	 */
	function __getFacebooktCount($url){
		try{
			$get_facebook = 'http://api.facebook.com/restserver.php?method=links.getStats&urls=' . urlencode($url);
			// 		$xml = file_get_contents($get_facebook);
			$xml = $this->__getHttpData($get_facebook);
			if(empty($xml)){
				return 0;
			}
			// 	_writeLog('--- facebook count ---');
			// 	_writeLog(print_r($xml , true));
			$xml = simplexml_load_string($xml);
			$likes = intval($xml->link_stat->total_count);
			if(empty($likes)) {
				$likes = 0;
			}
			return $likes;
		} catch (Exception $e) {
			throw $e;
		}
	}
	/**
	 * URLからはてぶ数取得
	 * @param type $url
	 * @return int
	 */
	function __getHatebuCount($url){
		try{
			$get_hatebu = 'http://api.b.st-hatena.com/entry.count?url=' . urlencode($url);
			$json = $this->__getHttpData($get_hatebu);
			//$this->_writeLog(print_r($json , true));

			if(empty($json)){
				return 0;
			}
			// 	_writeLog('--- twitter count ---');

			$json = json_decode($json);
			$count = (String)$json;
			if(empty($count)) {
				$count = 0;
			}
			return $count;
		} catch (Exception $e) {
			throw $e;
		}
	}
	/**
	 * curlで通信してurlの内容を取得します
	 * @param unknown_type $url
	 */
	function __getHttpData($url){
		try{
			$ch = curl_init();
			// 		curl_setopt($ch, CURLOPT_TIMEOUT_MS , '50000'); // タイムアウトは0.5秒
			curl_setopt($ch, CURLOPT_URL , $url); // 取得するURL
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 出力結果を加工しない
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		} catch (Exception $e) {
			throw $e;
		}
	}
	/**
	 * 記事のいいね数をカスタムフィールドに登録
	 * @param type $post_id
	 * @param type $tar_url
	 */
	function updateFacebook($post_id,$tar_url){
		try{
			$f_count = $this->__getFacebooktCount($tar_url);
			update_post_meta($post_id, 'f_count' , $f_count);
			$this->debugLog('f_count'.$f_count.'件');
		} catch (Exception $e) {
			echo "error";
			throw $e;
		}
	}
	/**
	 * 記事のはてぶ数をカスタムフィールドに登録
	 * @param type $post_id
	 * @param type $tar_url
	 */
	function updateHatebu($post_id,$tar_url){
		try{
			$h_count = $this->__getHatebuCount($tar_url);
			update_post_meta($post_id, 'h_count' , $h_count);
			$this->debugLog('h_count'.$h_count.'件');
		} catch (Exception $e) {
			echo "error";
			throw $e;
		}
	}
	/**
	 * 記事のツイート数をカスタムフィールドに登録
	 * @param type $post_id
	 * @param type $tar_url
	 */
	function updateTweet($post_id,$tar_url){
		try{
			$t_count = $this->__getTweetCount($tar_url);
			update_post_meta($post_id, 't_count' , $t_count);
			$this->debugLog('t_count'.$t_count.'件');
		} catch (Exception $e) {
			throw $e;
		}
	}
	/**
	 * Pファイル作成
	 * @return type
	 */
	function makePFile(){
		$fp = fopen($this->pfile_path, 'a');
		if($fp === FALSE) {
			return;
		}
		fwrite($fp, date('Y/m/d H:i:s') . ":makePFile");
		fclose($fp);
	}

	function Pfileexists(){
		return file_exists($this->pfile_path);
	}

}

?>
