<?php
//標準関数
if (!function_exists("xss")) {
	function xss($str = null){
		if(defined("FORM_CHARA_CODE")) {
			return htmlentities($str,ENT_QUOTES, FORM_CHARA_CODE);
		} else {
			return htmlentities($str,ENT_QUOTES, "UTF-8");
		}
	}
}

if (!function_exists("pr")) {
	function pr($var) {
		echo nl2br(print_r($var,true));
	}
}

if (!function_exists("am")) {
	function am($arr1 , $arr2){
		return array_merge($arr1 , $arr2);
	}
}
/**
 *
 * @param  array 追加の配列
 * @return array 都道府県の配列
 */
function getTodoufukenArray( $etc = array() ) {
    return am( array(
                 '北海道'	=>	"北海道",
                 '青森県'	=>	"青森県",
                 '岩手県'	=>	"岩手県",
                 '宮城県'	=>	"宮城県",
                 '秋田県'	=>	"秋田県",
                 '山形県'	=>	"山形県",
                 '福島県'	=>	"福島県",
                 '茨城県'	=>	"茨城県",
                 '栃木県'	=>	"栃木県",
                 '群馬県'	=>	"群馬県",
                 '埼玉県'	=>	"埼玉県",
                 '千葉県'	=>	"千葉県",
                 '東京都'	=>	"東京都",
                 '神奈川県'	=>	"神奈川県",
                 '新潟県'	=>	"新潟県",
                 '富山県'	=>	"富山県",
                 '石川県'	=>	"石川県",
                 '福井県'	=>	"福井県",
                 '山梨県'	=>	"山梨県",
                 '長野県'	=>	"長野県",
                 '岐阜県'	=>	"岐阜県",
                 '静岡県'	=>	"静岡県",
                 '愛知県'	=>	"愛知県",
                 '三重県'	=>	"三重県",
                 '滋賀県'	=>	"滋賀県",
                 '京都府'	=>	"京都府",
                 '大阪府'	=>	"大阪府",
                 '兵庫県'	=>	"兵庫県",
                 '奈良県'	=>	"奈良県",
                 '和歌山県'	=>	"和歌山県",
                 '鳥取県'	=>	"鳥取県",
                 '島根県'	=>	"島根県",
                 '岡山県'	=>	"岡山県",
                 '広島県'	=>	"広島県",
                 '山口県'	=>	"山口県",
                 '徳島県'	=>	"徳島県",
                 '香川県'	=>	"香川県",
                 '愛媛県'	=>	"愛媛県",
                 '高知県'	=>	"高知県",
                 '福岡県'	=>	"福岡県",
                 '佐賀県'	=>	"佐賀県",
                 '長崎県'	=>	"長崎県",
                 '熊本県'	=>	"熊本県",
                 '大分県'	=>	"大分県",
                 '宮崎県'	=>	"宮崎県",
                 '鹿児島県'	=>	"鹿児島県",
                 '沖縄県'	=>	"沖縄県" ), $etc);
}

/**
 * URLで利用できる文字列にエンコードする。
 *
 * @param string $key
 * @return string
 */
if (!function_exists("base64_urlencode")) {
	function base64_urlencode($key) {
	    $key = function_exists('bzcompress') ? bzcompress($key) : gzcompress($key);
	    return rtrim(strtr(base64_encode($key), '+/', '-_'),'=');
	}
}

/**
 * URLで利用できる文字列をデコードする。
 *
 * @param string $key
 * @return string
 */
if (!function_exists("base64_urldecode")) {
	function base64_urldecode($key) {
	    $key = base64_decode(strtr($key, '-_', '+/').str_repeat('=', (4-(strlen($key)%4))%4));
	    return function_exists('bzcompress') ? bzdecompress($key) : gzuncompress($key);
	}
}

/**
 *
 * @param integer $end    おわり
 * @param integer $start  はじまり
 * @param integer $it
 * @param string  $format 形式
 * @return array
 */
if (!function_exists("getNumArray")) {
	function getNumArray( $end, $start=0, $it=1, $format="%d" ) {
	    $array = array();
	    for( $i=$start; $i<=$end; $i+=$it ) {
	        $array[$i] = sprintf( $format, $i );
	    }
	    return $array;
	}
}