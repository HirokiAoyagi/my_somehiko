			<div id="one_column">
				<section id="contact">
					<h1><img src="<?php bloginfo('template_directory'); ?>/images/pages/ttl_contact.png" width="305" height="29" alt="お問い合わせ"></h1>
					<form method="POST" action="?" name="form" >

						<table>
							<tr>
								<th>お問い合わせ種別<span>必須</span></th>
								<td>
									<?php echo $this->select('contact_type' , array(
										'class' => 'fmselect' ,
										'option' => $this->config->contact_type
									));?>
									<br>
									<div class="clearfix radio_box">
										<?php foreach ($this->config->contact_group as $key => $val){
											echo $this->radio('contact_group' , array(
												'id' => 'Radio'.$key ,
												'class' => 'radio' ,
												'for' => 'Radio' .$key ,
												'value' => $key
											));
											echo '<label id="Label'.$key.'" for="Radio'.$key.'" class="radiolabel">'.$val.'</label>';
										}?>
									</div>

									<?php
									if(!empty($this->errors['contact_type']) || !empty($this->errors['contact_group'])) {
										if(!empty($this->errors['contact_type'])) {
											echo '<p class="error_mes">'.$this->errors['contact_type'][0].'</p>';
										} elseif(!empty($this->errors['contact_group'])) {
											echo '<p class="error_mes">'.$this->errors['contact_group'][0].'</p>';
										}
									}?>
								</td>
							</tr>
							<tr>
								<th>貴社名</th>
								<td>
									<?php
										echo $this->input('company_name',array(
											"type" => "text",
											'class' => 'input_style01' ,
											'placeholder'  => '株式会社インサイト"'
										));

									if(!empty($this->errors['company_name']) ) {
										echo '<p class="error_mes">'.$this->errors['company_name'][0].'</p>';
									}?>
								</td>
							</tr>
							<tr>
								<th>役職</th>
								<td>
									<?php
										echo $this->input('position',array(
											"type" => "text",
											'class' => 'input_style01' ,
											'placeholder'  => '営業"'
										));
									?>
								</td>
							</tr>
							<tr>
								<th>お名前<span>必須</span></th>
								<td>
									<?php
										echo $this->input('name',array(
											"type" => "text",
											'class' => 'input_style01' ,
											'placeholder'  => '渋谷太郎"'
										));

									if(!empty($this->errors['name'][0]) ) {
										echo '<p class="error_mes">'.$this->errors['name'][0].'</p>';
									}?>
								</td>
							</tr>
							<tr>
								<th>お名前（フリガナ）<span>必須</span></th>
								<td>
									<?php
										echo $this->input('kana',array(
											"type" => "text",
											'class' => 'input_style01' ,
											'placeholder'  => 'シブヤタロウ"'
										));

									if(!empty($this->errors['kana'][0]) ) {
										echo '<p class="error_mes">'.$this->errors['kana'][0].'</p>';
									}?>
								</td>
							</tr>
							<tr>
								<th>郵便番号</th>
								<td>
									<?php
										echo $this->input('zipcode',array(
											"type" => "text",
											'class' => 'input_style02' ,
											'placeholder'  => '000-0000',
											'id' => 'zip'
										));

										if(!empty($this->errors['zipcode'][0]) ) {
											echo '<p class="error_mes">'.$this->errors['zipcode'][0].'</p>';
										}?>
								</td>
							</tr>
							<tr>
								<th>ご住所</th>
								<td>
									<?php
										echo $this->input('add',array(
											"type" => "text",
											'class' => 'input_style01' ,
											'placeholder'  => '東京都渋谷区道玄坂1-19-9　第一暁ビルディング9F' ,
											'id' => 'addr'
										));

										if(!empty($this->errors['zipcode'][0]) ) {
											echo '<p class="error_mes">'.$this->errors['zipcode'][0].'</p>';
										}?>
								</td>
							</tr>
							<tr>
								<th>電話番号</th>
								<td>
									<?php
										echo $this->input('tel',array(
											"type" => "text",
											'class' => 'input_style01' ,
											'placeholder'  => '03-6416-0245'
										));
										if(!empty($this->errors['tel'][0]) ) {
											echo '<p class="error_mes">'.$this->errors['tel'][0].'</p>';
										}?>
								</td>
							</tr>
							<tr>
								<th>FAX番号</th>
								<td>
									<?php
										echo $this->input('fax',array(
											"type" => "text",
											'class' => 'input_style01' ,
											'placeholder'  => '03-6416-0245'
										));
										if(!empty($this->errors['fax'][0]) ) {
											echo '<p class="error_mes">'.$this->errors['fax'][0].'</p>';
										}?>
								</td>
							</tr>
							<tr>
								<th>メールアドレス<span>必須</span></th>
								<td>
									<?php
										echo $this->input('email',array(
											"type" => "text",
											'class' => 'input_style01' ,
											'placeholder'  => 'info@insight-co.jp'
										));
										if(!empty($this->errors['email'][0]) ) {
											echo '<p class="error_mes">'.$this->errors['email'][0].'</p>';
										}?>
								</td>
							</tr>
							<tr>
								<th>お問い合わせ内容<span>必須</span></th>
								<td>
									<?php
										echo $this->input('biko' , array(
											'type' => 'textarea' ,
											'placeholder' => 'ご意見・ご感想をご記入ください' ,
											'cols' => 40 ,
											'rows' => '4'
										));
										if(!empty($this->errors['biko'][0]) ) {
											echo '<p class="error_mes">'.$this->errors['biko'][0].'</p>';
										}?>
								</td>
							</tr>
							<tr>
								<th>個人情報取り扱い<span>必須</span></th>
								<td>
									ご入力いただいた個人情報は、当社のプライバシーポリシーに基づき、適切にお取り扱い致します。<br>
									<p class="mt15 map_link"><a href="<?php echo home_url('privacypolicy')?>" target="_blank">株式会社insightプライバシーポリシー</a></p><br>

										<input type="hidden" name="data[agree]" value="0" />
										<?php echo $this->checkbox('agree' , array('value' => '1' , 'id'=>'agree'));?><label for="agree">個人情報の取り扱いについて同意する</label>

										<?php
										if(!empty($this->errors['agree'][0]) ) {
											echo '<p class="error_mes">'.$this->errors['agree'][0].'</p>';
										}?>

								</td>
							</tr>
						</table>
						<div class="sub_box1"><input type="submit" value="入力内容を確認する" class="submit_style01"></div>
						<input type="hidden" name="_param[mode]"  value="confirm" />
					</form>
				</section>
			</div>

			<script src="<?php bloginfo('template_directory'); ?>/js/ajaxzip2/ajaxzip2.js"></script>
			<script src="<?php bloginfo('template_directory'); ?>/js/jquery.customSelect.min.js"></script>
			<script src="<?php bloginfo('template_directory'); ?>/js/ah-placeholder.js"></script>
			<script>
			$(function () {
				$('.fmselect').customSelect();

				$(".radio").change(function(){
					if($(this).is(":checked")){
						$(".RadioSelected:not(:checked)").removeClass("RadioSelected");
						$(this).next("label").addClass("RadioSelected");
					}
				});

				$(".radio").trigger('change');

				$('[placeholder]').ahPlaceholder({
					placeholderColor : 'silver',
					placeholderAttr : 'placeholder',
					likeApple : false
				});

				AjaxZip2.JSONDATA = '<?php bloginfo('template_directory'); ?>/js/ajaxzip2/data';

				$('#zip').on('keydown' , function(){
					AjaxZip2.zip2addr($(this).attr('name') , 'data[add]' , 'data[add]');
				});
			});
			</script>