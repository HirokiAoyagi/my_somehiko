<?php
/**
 * @package Post Ranking
 * @version 1.0
 */
/*
 Plugin Name: Post Ranking
Description: 1か月単位で記事のページ数を保存します。一応取り出す関数も用意しておきます。
Author: zuya@liginc
Version: 1.0
Author URI: http://liginc.co.jp
*/

define("POST_RANKING_DATE_TABLE_NAME", "post_date_rankings");
define("POST_RANKING_TABLE_NAME", "post_rankings");
define("POST_RANKING_CRON_SCHEDULE_HANDLER", 'post_ranking');

$objPostViewRanking = new postViewRanking();
// プラグインを有効化した時のフック
register_activation_hook(__FILE__, array($objPostViewRanking , 'activation_post_ranking'));

class postViewRanking {

	// 各種イベントをフックしていきます
	public function __construct() {

		// cronに設定
		add_action("init", array(&$this , "set_cron_event"));

		// カウント用のフック
		add_action('wp_head', array(&$this ,  'post_ranking_save_post_views'));
	}

	/**
	 * プラグインを有効かした時、tableがなかったら作成する
	 */
	function activation_post_ranking() {
		// プラグインを有効にしたときの処理を書く
		global $wpdb;

		$table_name = $wpdb->prefix.POST_RANKING_DATE_TABLE_NAME;

		if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {

			$query =
<<<EOF
CREATE TABLE IF NOT EXISTS `{$table_name}` (
  `post_id` int(11) NOT NULL,
  `target_date` date NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`target_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOF;

			$wpdb->query($query);
		}

		$table_name = $wpdb->prefix.POST_RANKING_TABLE_NAME;

		if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {

			$query =
<<<EOF
CREATE TABLE IF NOT EXISTS `{$table_name}` (
  `post_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOF;

			$wpdb->query($query);

			$this->log($wpdb->last_error);
		}

	}


	// 1日毎に集計用テーブルを更新する
	function update_date_ranking_table(){

		global $wpdb;

		// トランザクション開始
		$wpdb->query('BEGIN');

		// 1回現状のデータを削除
		$date_rank_table_name = $wpdb->prefix.POST_RANKING_DATE_TABLE_NAME;
		$all_rank_table_name = $wpdb->prefix.POST_RANKING_TABLE_NAME;

		if(!$wpdb->query("TRUNCATE TABLE `{$all_rank_table_name}`")){
			$wpdb->query('ROLLBACK');
			return;
		}

		// 1か月間のデータを取得して格納
		$before_monthe_date = date('Y-m-d' , strtotime("-1 month"));
		$yestaday_date = date('Y-m-d' , strtotime("-1 day"));
		$query =
<<<EOF
INSERT INTO $all_rank_table_name (post_id , count , updated)
	SELECT post_id , SUM(count) AS count , NOW() FROM $date_rank_table_name
	WHERE target_date >= %s AND target_date <= %s
	GROUP BY post_id
EOF;

		// insertを実行
		if(!$wpdb->query($wpdb->prepare($query, $before_monthe_date , $yestaday_date))){
			$wpdb->query('ROLLBACK');
			return;
		}


		// TODO 1年くらいに一応しておこうかな
		// 1か月以上前のランキングは不要なので削除
		$before_one_year_date = date('Y-m-d' , strtotime("-1 year"));
		$query = "DELETE FROM $date_rank_table_name WHERE target_date <= %s";

		if(!$wpdb->query($wpdb->prepare($query, $before_one_year_date ))){
			$wpdb->query('ROLLBACK');
			return;
		}


		$wpdb->query('COMMIT');

	}

	// wp-cron で実行させたい処理
	function cron_schedule_handler() {
		// ここに実際の処理を記述
		$this->update_date_ranking_table();

		wp_schedule_single_event($this->_nextExeTime(), POST_RANKING_CRON_SCHEDULE_HANDLER);
	}


	function set_cron_event(){

		// wp-cron でスケジュールされていなければ、スケジュール登録
		$crons = _get_cron_array();
		$enabled = false;
		foreach ( $crons as $time => $tasks ) {
			foreach ( $tasks as $procname => $task ) {
				if ($procname === POST_RANKING_CRON_SCHEDULE_HANDLER) {
					$enabled = true; break;
				}
			}
			if ($enabled) break;
		}
		if (!$enabled) {
			wp_schedule_single_event($this->_nextExeTime(), POST_RANKING_CRON_SCHEDULE_HANDLER);
		}
		unset($tasks); unset($crons);

		// スケジュール実行
		add_action(POST_RANKING_CRON_SCHEDULE_HANDLER, array(&$this , 'cron_schedule_handler'));

	}

	private function _nextExeTime(){
		// 夜中の1時に動くように設定する
		$exeTime = mktime('1' , '0' , '0' , date('m') , date('d') , date('Y')) - 3600 * 9;
		return $exeTime;
	}


	/**
	 * singleページが呼び出された際に、カウントする
	 */
	function post_ranking_save_post_views(){
		if(is_single()) {
			global $post , $wpdb;


			$post_id = $post->ID;
			$target_date = date('Y-m-d');

			$table_name = $wpdb->prefix.POST_RANKING_DATE_TABLE_NAME;

			// すでにデータがあるか判定
			$count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$table_name} WHERE target_date = %s AND post_id= %s"  , $target_date  , $post_id));

			if($count == 0) {
				$wpdb->insert($table_name , array(
						'target_date' => $target_date , 'post_id' => $post_id , 'count' => 1
				));
			} else {
				$query = "UPDATE $table_name SET count = count + 1 WHERE target_date = %s AND post_id= %s";
				$wpdb->query($wpdb->prepare($query , $target_date , $post_id));
			}
		}
	}

	function log($output, $path = null) {
		if(WP_DEBUG) {
			$str = var_export($output, true);
			$path = isset($path) ? $path : dirname(__FILE__).'/log/debug.log';
			$fp = fopen($path, 'w');
			fwrite($fp, "{$str}\n");
			fclose($fp);
		}
	}
}

/**
 * ランキングのデータを取得します
 * @param 最大取得件数 $limit
 */
function _get_page_view_ranking($limit = 5){
	global $wpdb;

	$all_rank_table_name = $wpdb->prefix.POST_RANKING_TABLE_NAME;

	$query = "SELECT {$wpdb->posts}.* FROM {$all_rank_table_name} INNER JOIN {$wpdb->posts} ON {$all_rank_table_name}.post_id = {$wpdb->posts}.ID WHERE {$wpdb->posts}.post_status = 'publish' AND {$wpdb->posts}.post_type = 'post' ORDER BY {$all_rank_table_name}.count DESC LIMIT %d";

	return $wpdb->get_results($wpdb->prepare($query , $limit));

}
