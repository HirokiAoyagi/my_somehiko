<?php
/**
 * WordPress全体で共通して使用する処理を記載します。
 *
 *
 */
	if ( WP_DEBUG ) {
		function pr( $vars ) {
			echo '<pre>';
			print_r( $vars );
			echo '</pre>';
		}
	}

	/**
	 * 抜粋の文字数設定
	 * @param unknown_type $length
	 * @return number
	 * @author danda hayato
	 * @create 2013/09/12
	 * @version    1.0
	 */
	function set_excerpt_mblength($length) {
		return 59;
	}
	//add_filter('excerpt_mblength', 'set_excerpt_mblength');

	/**
	 * 抜粋の文末変更
	 * @param unknown_type $more
	 * @return string
	 * @author danda hayato
	 * @create   2013/09/12
	 * @version    1.0
	 */
	function set_excerpt_more($more) {
		return '...';
	}
	//add_filter('excerpt_more', 'set_excerpt_more');

	/**
	 * 指定のスラッグがURIに含まれているか確認する。
	 * 主に静的ページのチェックに使ってください。
	 * @param unknown_type $slug
	 * @return boolean
	 * @author danda hayato
	 * @create  2013/09/12
	 * @version    1.0
	 */
	function is_static_page($slug = ''){
		if(strstr($_SERVER["REQUEST_URI"],$slug)):
			return true;
		endif;
			return false;
	}

	/**
	 *
	 * ファイル保存しているカスタムフィールドからファイルリンクを取得する
	 * @param unknown_type $postid 記事ID
	 * @param unknown_type $key ファイルを保持しているカスタムフィールドキー
	 */
	function get_customfield_filelink($postid,$key){
		$files = get_post_meta($postid, $key, false);
		foreach($files as $file):
			$file = wp_get_attachment_url($file);
			return $file;
		endforeach;
	}

	
	/**
	 * 投稿が指定期間以内かチェックする
	 * @param type $post_id 記事ID
	 * @param type $time 期間指定　strtotimeのフォーマットを指定
	 * @return boolean
	 */
	function is_newpost( $post_id = null,$time=NEW_POST_TIME){
		$dt = new DateTime();
		$dt->setTimeZone(new DateTimeZone('Asia/Tokyo'));
		$today = get_post_time('Y-m-d', false, $post_id );
		$limit_day = date( "Y-m-d", strtotime( $time ) );
		if ( strtotime($today) >= strtotime($limit_day)) :
			return true;
		endif;
		return false;
	}
	
	/**
	 * エンコード
	 * @param unknown_type $str
	 * @author danda hayato
	 * @create
	 * @version    1.0
	 */
	function xss($str = null){
		return htmlentities($str,ENT_QUOTES, "UTF-8");
	}